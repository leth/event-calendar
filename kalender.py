from fpdf import FPDF
import calendar
import os
import time
import urllib
import json
import csv
from pprint import pprint
#import datetime
from datetime import datetime
from zoneinfo import ZoneInfo
from dateutil.parser import *

# october 2022 has max amount of weeks possible
year = 2023
month = 3
# the file used for input that is not on radar
additional_file="additional.csv"
# Files containing data from radar
data_file="data.json"
venues_file="venues.json"
url="https://radar.squat.net/api/1.2/search/events.json?facets[city][]=Stockholm"
cal = calendar.Calendar()
# format =A2 size in mm
pdf = FPDF(orientation = 'L', unit='mm', format = [420, 594])
events = {}
date_size = (78, 58)

def download_data():
    url_request = urllib.request.urlopen(url)
    print("Updating Data ", end='')
    with open(data_file, "wb") as file:
        while True:
            buffer = url_request.read(1024)
            if not buffer:
                break
            file.write(buffer)
            print(".", end='')
    print(" done")

def translate_venue_id(saved_venues, save_venue_data, venue_data):
    if venue_data['id'] in saved_venues:
        return saved_venues[venue_data['id']], save_venue_data
    else:
        print("Unknown venue!")
        print("Venue id: ", end='')
        pprint(venue_data)
        venue_name = input('Venue name: ')
        saved_venues[venue_data['id']] = venue_name
        save = input('Save venue name in file ([y]es/*): ')
        if save.lower() in ['yes', 'y']:
            if not save_venue_data:
                save_venue_data = saved_venues
            save_venue_data[venue_data['id']] = venue_name
        pprint(save_venue_data)
        return venue_name, save_venue_data

def insert_event(title, venue, start_datetime, end_datetime):
    date = start_datetime.date()
    if not date.day in events:
        events[date.day] = []
    events[date.day].append((title, venue, start_datetime, end_datetime))

def interprete_event(event, venue_data, save_venue_data):
    title = event['title']
    start_datetime = datetime.fromisoformat(event['date_time'][0]['time_start'])
    end_datetime = datetime.fromisoformat(event['date_time'][0]['time_end'])
    date = start_datetime.date()
    if date.month != month or date.year != year:
        return save_venue_data
    venue, save_venue_data = translate_venue_id(venue_data,
                                                save_venue_data,
                                                event['offline'][0])
    insert_event(title, venue, start_datetime, end_datetime)
    return save_venue_data

def read_calendar_data():
    with open(data_file, "r") as file:
        calendar_data = json.load(file)
    if os.path.isfile(venues_file):
        with open(venues_file, "r") as file:
            venue_data = json.load(file)
    else:
        venue_data = {}
    save_venue_data = {}
    for event in calendar_data['result'].items():
        save_venue_data = interprete_event(event[1], venue_data, save_venue_data)
    # if we have saved any new venue names then save to file
    if save_venue_data:
        with open(venues_file, "w") as file:
            json.dump(save_venue_data, file)

def read_additional_data():
    with open(additional_file, newline='') as additional_data:
        additional_reader = csv.reader(additional_data, dialect='unix')
        for row in additional_reader:
            # ['title', 'venue', 'start date_time', 'end date_time']
            insert_event(row[0], row[1], parse(row[2]), parse(row[3]))

def print_info_box():
    pdf.set_draw_color(64, 8, 8)
    pdf.set_text_color(64, 8, 8)
    pdf.set_font('Arial', '', 18)
    pdf.text(300, 15, 'Contact info:')
    pdf.rect(340, 7, 240, 32)
    pdf.text(344, 15, 'Allt Åt Alla: alltatalla.se')
    pdf.text(344, 22, 'Cyklopen: cyklopen.se')
    pdf.text(344, 29, 'Kafé 44: kafe44.org')
    pdf.text(344, 36, 'A22: @a22stockholm')
    pdf.text(422, 15, 'Nattsvart Verkstad: nattsvartverkstad.noblogs.org')
    pdf.text(422, 22, 'Solidaria: solidaria_initiative@riseup.net')
    pdf.text(422, 29, 'Ingen Människa Är Illegal: stockholm@ingenillegal.org')
    pdf.text(422, 36, 'Stockholm Anarkistiska Förening: s_a_f@riseup.net')

def draw_date_events(date, x, y):
    if not date.day in events:
        return
    if date.month != month:
        return
    event_on_day = 0
    for event in events[date.day]:
        ## event: [ title, venue, start_datetime, end_datetime ]
        # venue + time
        pdf.set_font('Arial', 'B', 13)
        if event[2] == event[3]:
            text = event[1] + " " + event[2].strftime("%H:%M")
        else:
            start_time = event[2].strftime("%H") \
                if event[2].strftime("%M") == "00" \
                   else event[2].strftime("%H:%M")
            end_time = event[3].strftime("%H") \
                if event[3].strftime("%M") == "00" \
                   else event[3].strftime("%H:%M")
            text = event[1] + " " + start_time + "-" + end_time
        pdf.text(x+2, y+event_on_day*12+6, text)
        # title
        pdf.set_font('Arial', '', 13)
        info_text = event[0].split('\\n')
        line=0
        for text_line in info_text:
            pdf.text(x+2, y+event_on_day*12+11+line, text_line)
            line+=4
        event_on_day += 1

def draw_date(date, x, y):
    if (date.month != month):
        pdf.set_draw_color(192, 192, 192)
        pdf.set_text_color(192, 192, 192)
    pdf.rect(x, y, date_size[0], date_size[1])
    pdf.set_font('Arial', 'B', 20)
    date_string = str(date.day)
    pdf.text(x+date_size[0]-2-len(date_string)*4, y+7, date_string)
    if (date.month != month):
        pdf.set_draw_color(0, 0, 0)
        pdf.set_text_color(0, 0, 0)
        if (date.month > month):
            if (date.day == 1):
                pdf.line(x, y, x, y+date_size[1])
            pdf.line(x, y, x+date_size[0], y)
    draw_date_events(date, x, y)

def draw_week(week, y, dates):
    pdf.set_font('Arial', 'B', 12)
    pdf.text(16, y+4, "v."+str(week))
    for i in range(7):
        draw_date(dates[i], date_size[0]*i+26, y)

def draw_calendar():
    weeks = cal.monthdatescalendar(year, month)
    pdf.add_page()
    ## print out the Month in the top left
    pdf.set_font('Arial', 'B', 102)
    month_text = weeks[1][0].strftime("%B")
    pdf.text(25, 38, month_text)
    ## print out the link to radar
    # figure out where month ends
    link_start = 25 + pdf.get_string_width(month_text) + 5
    pdf.set_font('Arial', '', 20)
    pdf.text(link_start, 16, "Also online at:")
    pdf.set_font('Arial', '', 32)
    pdf.text(link_start, 27, "https://gnistor.se")
    pdf.text(link_start, 38, "https://radar.squat.net")

    # print out weeks in order:
    for week in range(len(weeks)):
        draw_week(week+39, date_size[1]*week+52, weeks[week])

    print_info_box()

    pdf.output('kalender.pdf', 'F')

if os.path.exists(data_file):
    if os.path.getmtime(data_file) + 86400 < time.time():
        download_data()
else:
    download_data()

read_calendar_data()

read_additional_data()

draw_calendar()
